package br.com.trabalho.models;

import java.util.Calendar;
import java.util.Date;

public class Reserva {
	
	private Usuario usuario;
	private Date dataReserva;
	private Livro livro;

	public Reserva(Usuario usuario, Livro livro) throws Exception {
		this.usuario = usuario;
		this.livro = livro;
		this.usuario.adicionaReserva(this);
		this.livro.adicionaReserva(this);
		this.setDataReserva();
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public Date getDataReserva() {
		return dataReserva;
	}

	public void setDataReserva() {
		Calendar cal = Calendar.getInstance();
		this.dataReserva = cal.getTime();
	}
	
}
