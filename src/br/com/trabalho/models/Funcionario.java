package br.com.trabalho.models;

public class Funcionario extends Usuario {

	@Override
	public boolean getMaxEmprestimos() {
		if(this.getEmprestimosAtivos().size() == 3) {
			return true;
		}
		return false;
	}

	@Override
	public boolean temPrivilegio() {
		return false;
	}
	
	@Override
	public int calculaTempoDeUso() {
		return 2;
	}
}
