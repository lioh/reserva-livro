package br.com.trabalho.models;

public interface Observer {
	
	int getContNotificacoes();
	void setContNotificacoes(int contNotificacoes);
	void atualiza();
}
