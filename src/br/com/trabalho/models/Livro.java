package br.com.trabalho.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.trabalho.errors.EmprestimoException;
import br.com.trabalho.errors.ReservaException;

public class Livro implements Subject{
	
	private String codigo;
	private String titulo;
	private String editora;
	private String edicao;
	private String anoPublicacao;	
	private List<Observer> observes = new ArrayList<Observer>();
	private List<String> autores = new ArrayList<String>();
	private List<Exemplar> exemplares = new ArrayList<Exemplar>();
	private List<Emprestimo> emprestimos = new ArrayList<Emprestimo>();
	private List<Reserva> reservas = new ArrayList<Reserva>();
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getEditora() {
		return editora;
	}
	
	public void setEditora(String editora) {
		this.editora = editora;
	}
	
	public List<String> getAutores() {
		return autores;
	}
	
	public void setAutores(List<String> autores) {
		this.autores = autores;
	}
	
	public void adicionaAutores(String autor) {
		this.autores.add(autor);
	}
	
	public void removeAutores(String autor) {
		this.autores.remove(autor);
	}
		
	public String getEdicao() {
		return edicao;
	}
	
	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}
	
	public String getAnoPublicacao() {
		return anoPublicacao;
	}
	
	public void setAnoPublicacao(String anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}
	
	public Exemplar processaEmprestimo(Usuario usuario) throws Exception {
		if(this.getExemplaresDisponiveis().isEmpty()) {
			throw new EmprestimoException("Livro não possui exemplares disponiveis.");
		} 
		
		if(usuario.podeEmprestar(this)) {
			if(usuario.isReservado(this) || usuario.temPrivilegio() ||
					getReservas().size() < getExemplaresDisponiveis().size()) {
				return this.getExemplarDisponivel();
			}
		}
		throw new EmprestimoException("Usuario não pode pegar o livro emprestado, pois não reservou.");
	}
	
	public boolean processaReserva(Usuario usuario) throws Exception {
		if(usuario.isEmprestado(this) || usuario.isReservado(this) || 
				usuario.getMaxReservas()) {
			throw new ReservaException("Usuario já reservou ou "
					+ "possui um exemplar emprestado desse livro.");
		}
		if(this.getExemplaresDisponiveis().isEmpty()) {
			return true;
		} 	
		return false;		
	}
	
	public List<Exemplar> getExemplares() {
		return exemplares;
	}
	
	public void setExemplares(List<Exemplar> exemplares) {
		this.exemplares = exemplares;
	}
	
	public void adicionaExemplar(Exemplar exemplar) {
		this.exemplares.add(exemplar);
	}	
	
	public List<Exemplar> getExemplaresByStatus(StatusExemplar status) {
		List<Exemplar> exemplaresPorStatus = new ArrayList<>();
		for (Exemplar exemplar : this.exemplares) {
			if(exemplar.getStatus() == status) {
				exemplaresPorStatus.add(exemplar);
			}
		}
		return exemplaresPorStatus;
	}
	
	public List<Exemplar> getExemplaresDisponiveis() {
		return this.getExemplaresByStatus(StatusExemplar.DISPONIVEL);
	}
	
	public List<Exemplar> getExemplaresEmprestados() {
		return this.getExemplaresByStatus(StatusExemplar.EMPRESTADO);
	}

	public void verificaAtraso() {
		for (Emprestimo emprestimo : this.getEmprestimos()) {
			Date date = emprestimo.getDataPrevistaDevolucao();
			if(date.before(Calendar.getInstance().getTime())) {
				emprestimo.getUsuario().setStatus(StatusUsuario.DEVEDOR);
			}
		}
	}
	
	public Exemplar getExemplarDisponivel() {
		return this.getExemplaresDisponiveis().get(0);	
	}
	
	public Exemplar getExemplarEmprestado() {
		return this.getExemplaresEmprestados().get(0);
	}
	
	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}
	
	public void setEmprestimos(List<Emprestimo> emprestimos) {
		this.emprestimos = emprestimos;
	}
	
	public void adicionaEmprestimo(Emprestimo emprestimo) {
		this.emprestimos.add(emprestimo);
	}

	public List<Reserva> getReservas() {
		return reservas;
	}
	
	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}
	
	public void adicionaReserva(Reserva reserva) {
		this.reservas.add(reserva);
		this.atualizaObservadores();
	}
	
	@Override
	public String toString() {
		return "Livro [Código = " + codigo + ", Titulo = " + titulo + ", Editora = " + editora + ", Edição = " + edicao
				+ ", Ano de Publicacao = " + anoPublicacao + ", Autores = " + autores + ", Exemplares=" + exemplares + "]";
	}

	@Override
	public void registraObservador(Observer observer) {
		this.observes.add(observer);
	}

	@Override
	public void removeObservador(Observer observer) {
		this.observes.remove(observer);
	}

	@Override
	public void notificaObservadores() {
		for (Observer observer : observes) {
			observer.atualiza();
		}
	}

	public List<Observer> getObserves() {
		return observes;
	}

	public void setObserves(List<Observer> observes) {
		this.observes = observes;
	}
	
	public void atualizaObservadores() {
		if(this.getReservas().size() > 2) {
			this.notificaObservadores();
		}
	}
}
