package br.com.trabalho.models;

import java.util.Calendar;
import java.util.Date;

public class Emprestimo {
	
	private Usuario usuario;
	private Exemplar exemplar;
	private Date dataEmprestimo;
	private Date dataPrevistaDevolucao;
	private Date dataDevolucao;
	private StatusEmprestimo status;

	public Emprestimo(Usuario usuario, Exemplar exemplar) throws Exception {
		this.usuario = usuario;
		this.exemplar = exemplar;
		this.exemplar.setStatus(StatusExemplar.EMPRESTADO);
		this.setDataEmprestimo();
		this.setDataPrevistaDevolucao();
		this.setStatus(StatusEmprestimo.ATIVO);
		Livro livro = this.exemplar.getLivro();
		usuario.adicionaEmprestimo(this);
		livro.adicionaEmprestimo(this);
		if(usuario.isReservado(livro)) {
			livro.getReservas().remove(usuario.getLivroReservado(livro));
			usuario.getReservas().remove(usuario.getLivroReservado(livro));
		}
	}
	
	public Exemplar getExemplar() {
		return exemplar;
	}

	public void setExemplar(Exemplar exemplar) {
		this.exemplar = exemplar;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Date getDataDevolucao() {
		return this.dataDevolucao;
	}
	
	public void setDataDevolucao() {
		Calendar cal = Calendar.getInstance();
		this.dataDevolucao = cal.getTime();
	}
	
	public void setDataEmprestimo() {
		Calendar cal = Calendar.getInstance();
		this.dataEmprestimo = cal.getTime();
	}
	
	public Date getDataEmprestimo() {
		return dataEmprestimo;
	}

	public Date getDataPrevistaDevolucao() {
		return dataPrevistaDevolucao;
	}

	public void setDataPrevistaDevolucao() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) + this.usuario.calculaTempoDeUso());
		this.dataPrevistaDevolucao = cal.getTime();
	}

	public StatusEmprestimo getStatus() {
		return status;
	}

	public void setStatus(StatusEmprestimo status) {
		this.status = status;
	}
}
