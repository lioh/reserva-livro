package br.com.trabalho.models;

public class Exemplar {
	
	private String codigo;
	private Livro livro;
	private StatusExemplar status;
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
		this.livro.adicionaExemplar(this);
	}
	
	public boolean isDisponivel() {
		if(!getStatus().equals(StatusExemplar.DISPONIVEL)) {			
			return false;
		}
		return true;
	}

	public StatusExemplar getStatus() {
		return status;
	}
	
	public void setStatus(StatusExemplar status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Exemplar [Código = " + codigo + ", Livro = " + livro.getCodigo() + ", Status = " + status + " ]";
	}
	
}
