package br.com.trabalho.models;

public enum StatusEmprestimo {
	ATIVO, FINALIZADO;
}
