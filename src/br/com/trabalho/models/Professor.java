package br.com.trabalho.models;

public class Professor extends Usuario implements Observer {
	
	private int contNotificacoes;
	
	@Override
	public int getContNotificacoes() {
		return contNotificacoes;
	}
	
	@Override
	public void setContNotificacoes(int contNotificacoes) {
		this.contNotificacoes = contNotificacoes;
	}
	
	@Override
	public boolean getMaxEmprestimos() {
		return false;
	}

	@Override
	public boolean temPrivilegio() {
		return true;
	}
	
	@Override
	public int calculaTempoDeUso() {
		return 7;
	}
	
	@Override
	public void atualiza() {
		this.setContNotificacoes(this.getContNotificacoes() + 1);		
	}
}
