package br.com.trabalho.models;

import java.util.ArrayList;
import java.util.List;

import br.com.trabalho.errors.DevolveException;
import br.com.trabalho.helpers.UsuarioHelper;

public abstract class Usuario {

	private String codigo;
	private String nome;
	private StatusUsuario status = StatusUsuario.REGULAR;
	private List<Emprestimo> emprestimos = new ArrayList<Emprestimo>();
	private List<Reserva> reservas = new ArrayList<Reserva>();
	private UsuarioHelper usuarioHelper;

	public void setUsuarioHelper(UsuarioHelper usuarioHelper) {
		this.usuarioHelper = usuarioHelper;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() { 
		return codigo;
	}
	
	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void setEmprestimos(List<Emprestimo> emprestimos) {
		this.emprestimos = emprestimos;
	}
	
	public List<Emprestimo> getEmprestimoByStatus(StatusEmprestimo status) {
		List<Emprestimo> emprestimosPorStatus = new ArrayList<>();
		for (Emprestimo emprestimo : this.emprestimos) {
			if(emprestimo.getStatus() == status) {
				emprestimosPorStatus.add(emprestimo);
			}
		}
		return emprestimosPorStatus;
	}
	
	public List<Emprestimo> getEmprestimoFinalizado(){
		return this.getEmprestimoByStatus(StatusEmprestimo.FINALIZADO);
	}
	
	public List<Emprestimo> getEmprestimosAtivos(){
		return this.getEmprestimoByStatus(StatusEmprestimo.ATIVO);
	}
	
	public boolean podeEmprestar(Livro livro) throws Exception {
		return this.usuarioHelper.podeEmprestar(livro);
	}
	
	public void adicionaEmprestimo(Emprestimo emprestimo) {
		this.emprestimos.add(emprestimo);
	}
	
	public void devolveEmprestimo(Emprestimo emprestimo) throws DevolveException {
		if(emprestimo == null) {
			throw new DevolveException("Usuario não realizou emprestimo de nenhum exemplar");
		}
		emprestimo.setStatus(StatusEmprestimo.FINALIZADO);
		emprestimo.setDataDevolucao();
		Livro livro = emprestimo.getExemplar().getLivro();
		livro.getEmprestimos().remove(emprestimo);
		Exemplar exemplar = emprestimo.getExemplar();
		exemplar.setStatus(StatusExemplar.DISPONIVEL);
	}

	public List<Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}
	
	public void adicionaReserva(Reserva reserva) {
		this.reservas.add(reserva);
	}

	public StatusUsuario getStatus() {
		return status;
	}
	
	public boolean isDevedor() {
		if(this.getStatus().equals(StatusUsuario.DEVEDOR)) {
			return true;
		}
		return false;
	}

	public void setStatus(StatusUsuario status) {
		this.status = status;
	}
	
	public boolean isEmprestado(Livro livro) {
		for (Emprestimo emprestimo : this.getEmprestimosAtivos()) {
			Livro livroEmprestado = emprestimo.getExemplar().getLivro();
			if(livroEmprestado.equals(livro)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isReservado(Livro livro) {
		for (Reserva reserva : reservas) {
			Livro livroReservado = reserva.getLivro();
			if(livroReservado.equals(livro)) {
				return true;
			}
		}
		return false;
	}
	
	public Emprestimo getLivroEmprestado(Livro livro) {
		for (Emprestimo emprestimo : this.getEmprestimosAtivos()) {
			Livro livroEmprestado = emprestimo.getExemplar().getLivro();
			if(livroEmprestado.equals(livro)) {
				return emprestimo;
			}
		}
		return null;
	}
	
	public Reserva getLivroReservado(Livro livro) {
		for (Reserva reserva : reservas) {
			Livro livroResevado = reserva.getLivro();
			if(livroResevado.equals(livro)) {
				return reserva;
			}
		}
		return null;
	}

	public boolean getMaxReservas() {
		if(this.getReservas().size() >= 3) {
			return true;
		}
		return false;
	}
	
	public abstract int calculaTempoDeUso();
	public abstract boolean getMaxEmprestimos();
	public abstract boolean temPrivilegio();
	
}