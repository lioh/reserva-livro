package br.com.trabalho.commands;

import java.util.Scanner;

public class ConsultarUsuarioCommand extends CodigoCommand {
	public ConsultarUsuarioCommand() {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.carregaDados(entrada);
		controller.consultaUsuario(codigo);
	}
}

