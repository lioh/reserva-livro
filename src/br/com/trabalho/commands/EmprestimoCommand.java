package br.com.trabalho.commands;

import java.util.Scanner;

public class EmprestimoCommand extends UsuarioLivroCommand{
		
	public EmprestimoCommand() {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.execute(entrada);
		controller.emprestar(codigoUsuario, codigoLivro);
	}
}
