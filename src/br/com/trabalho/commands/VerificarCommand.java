package br.com.trabalho.commands;

import java.util.Scanner;

import br.com.trabalho.controllers.AplicacaoController;

public class VerificarCommand implements Command {
	protected AplicacaoController controller;
	
	public VerificarCommand() {
		this.controller = AplicacaoController.getController();
	}
	
	@Override
	public void execute(Scanner entrada) {
		controller.verificarAtraso();
	}
}
