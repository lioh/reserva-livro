package br.com.trabalho.commands;

import java.util.Scanner;

public class DevolverCommand extends UsuarioLivroCommand {

	public DevolverCommand() {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.carregaDados(entrada);
		controller.devolver(codigoUsuario, codigoLivro);
	}
}
