package br.com.trabalho.commands;

import java.util.Scanner;

public class ObservarCommand extends UsuarioLivroCommand {
	
	public ObservarCommand () {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.carregaDados(entrada);
		controller.observar(codigoUsuario, codigoLivro);
	}
}
