package br.com.trabalho.commands;

import java.util.Scanner;

public class SaiCommand implements Command {
	
	@Override
	public void execute(Scanner entrada) {
		System.out.println("");
		System.out.println("Foi muito bom o tempo que passamos juntos, que pena que tem que sair!");
		System.out.println("Tchau Tchau e volte sempre o/");
		System.exit(0);	
	}

}
