package br.com.trabalho.commands;

import java.util.Scanner;

public class NotificarCommand extends CodigoCommand {
	public NotificarCommand() {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.carregaDados(entrada);
		controller.consultaNotificacaoUsuario(codigo);
	}
}
