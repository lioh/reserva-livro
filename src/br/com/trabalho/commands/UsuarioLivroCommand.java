package br.com.trabalho.commands;

import java.util.Scanner;

import br.com.trabalho.controllers.AplicacaoController;

public class UsuarioLivroCommand implements Command{
	protected AplicacaoController controller;
	protected String codigoUsuario;
	protected String codigoLivro;
	
	public UsuarioLivroCommand() {
		this.controller = AplicacaoController.getController();
	}

	@Override
	public void execute(Scanner entrada) {
		this.carregaDados(entrada);
	}
	
	public void carregaDados(Scanner entrada) {
		this.codigoUsuario = entrada.next();
		this.codigoLivro = entrada.next();
	}
}
