package br.com.trabalho.commands;

import java.util.Scanner;

public class ReservaCommand extends UsuarioLivroCommand{
		
	public ReservaCommand() {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.carregaDados(entrada);
		controller.reservar(codigoUsuario, codigoLivro);
	}
}
