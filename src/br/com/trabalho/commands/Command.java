package br.com.trabalho.commands;

import java.util.Scanner;

public interface Command {
	public void execute(Scanner entrada);
}
