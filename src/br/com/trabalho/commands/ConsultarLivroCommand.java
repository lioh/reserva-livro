package br.com.trabalho.commands;

import java.util.Scanner;

public class ConsultarLivroCommand extends CodigoCommand {
	public ConsultarLivroCommand() {
		super();
	}
	
	@Override
	public void execute(Scanner entrada) {
		super.carregaDados(entrada);
		controller.consultaLivro(codigo);
	}
}
