package br.com.trabalho.commands;

import java.util.Scanner;

import br.com.trabalho.controllers.AplicacaoController;

public class CodigoCommand implements Command {
	protected AplicacaoController controller;
	protected String codigo;
	
	public CodigoCommand() {
		this.controller = AplicacaoController.getController();
	}

	@Override
	public void execute(Scanner entrada) {
		this.carregaDados(entrada);
	}
	
	public void carregaDados(Scanner entrada) {
		this.codigo = entrada.next();
	}
}
