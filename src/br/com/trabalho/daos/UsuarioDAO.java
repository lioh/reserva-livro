package br.com.trabalho.daos;

import java.util.ArrayList;
import java.util.List;

import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Reserva;
import br.com.trabalho.models.Usuario;

public class UsuarioDAO {
	private static List<Usuario> usuarios = new ArrayList<Usuario>();
	
	public static void adicionaUsuario(Usuario u) {
		usuarios.add(u);
	}

	public static void removeUsuario(Usuario u) {
		usuarios.remove(u);
	}
	
	public static List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public static Usuario buscaUsuarioCodigo(String codigo) {
		for (Usuario usuario : usuarios) {
			if(usuario.getCodigo().equals(codigo)) {
				return usuario;
			}
		}
		return null;
	}
	
	public static void consultaUsuario(String codigo) {
		Usuario usuario = buscaUsuarioCodigo(codigo);
		System.out.println("---------------------------");
		System.out.println(usuario.getNome());
		System.out.println("Lista de Emprestimos:");
		for (Emprestimo emprestimo : usuario.getEmprestimosAtivos()) {
			System.out.println("Titulo: "+ emprestimo.getExemplar().getLivro().getTitulo());
			System.out.println("Data de Emprestimo: "+ emprestimo.getDataEmprestimo());
			System.out.println("Status: "+ emprestimo.getStatus());
			System.out.println("Data para Devolução: "+ emprestimo.getDataPrevistaDevolucao());
		}
		for(Emprestimo emprestimo : usuario.getEmprestimoFinalizado()) {
			System.out.println("Titulo: "+ emprestimo.getExemplar().getLivro().getTitulo());
			System.out.println("Data de Emprestimo: "+ emprestimo.getDataEmprestimo());
			System.out.println("Status: "+ emprestimo.getStatus());
			System.out.println("Data de Devolução: "+ emprestimo.getDataDevolucao());
		}	
		System.out.println("Lista de Reservas:");
		for (Reserva reserva : usuario.getReservas()) {
			System.out.println("Titulo: "+ reserva.getLivro().getTitulo());
			System.out.println("Data de Reserva: "+ reserva.getDataReserva());
		}
		System.out.println("---------------------------");
	}
}
