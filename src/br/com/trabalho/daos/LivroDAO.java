package br.com.trabalho.daos;

import java.util.ArrayList;
import java.util.List;

import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Observer;
import br.com.trabalho.models.Reserva;
import br.com.trabalho.models.StatusExemplar;
import br.com.trabalho.models.Usuario;

public class LivroDAO {
	
	private static List<Livro> livros = new ArrayList<Livro>();
	
	public static void adicionaLivro(Livro l) {
		livros.add(l);
	}

	public static void removeLivro(Livro l) {
		livros.remove(l);
	}
	
	public static List<Livro> getLivros() {
		return livros;
	}
	
	public static Livro buscaLivroCodigo(String codigo) {
		for (Livro livro : livros) {
			if(livro.getCodigo().equals(codigo)) {
				return livro;
			}
		}
		return null;
	}
	
	public static void consultaLivro(String codigo) {
		Livro livro = buscaLivroCodigo(codigo);
		System.out.println("---------------------------");
		System.out.println("Titulo: "+ livro.getTitulo());
		System.out.println("Quantidade de Reservas: "+ livro.getReservas().size());
		if(!livro.getReservas().isEmpty()) {
			for (Reserva reserva : livro.getReservas()) {
				System.out.println("Reservado por: "+reserva.getUsuario().getNome());
			}
		}
		System.out.println("Exemplares:");
		System.out.println("Codigo   Status");
		for (Exemplar exemplar : livro.getExemplares()) {
			System.out.println(exemplar.getCodigo() + "       "
					+ exemplar.getStatus());
			if(exemplar.getStatus().equals(StatusExemplar.EMPRESTADO)) {
				for (Emprestimo emprestimo : livro.getEmprestimos()) {
					if(emprestimo.getExemplar().equals(exemplar)) {
						System.out.println("Nome: "+emprestimo.getUsuario().getNome());
						System.out.println("Data de empréstimo: "+emprestimo.getDataEmprestimo());
						System.out.println("Data prevista para devolução: "+emprestimo.getDataPrevistaDevolucao()); 
					}
				}
			}
		}
		System.out.println("---------------------------");
	}
	

	@SuppressWarnings("unlikely-arg-type")
	public static int consultaNotificacaoUsuario(Usuario usuario) {
		for (Livro livro : livros) {
			for (Observer observer : livro.getObserves()) {
				if(usuario.equals(observer)) {					
					return observer.getContNotificacoes();
				}
			}
		}
		return 0;
	}

}
