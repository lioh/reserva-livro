package br.com.trabalho.tests;

import org.junit.jupiter.api.Test;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;
import org.junit.Assert;
import junit.framework.TestCase;

public class ProfessorTest extends TestCase{
	
	@Test
	public void TemEmprestimosIlimitados() {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("100");
		Assert.assertEquals(false, usuario.getMaxEmprestimos());
	}
	
	@Test
	public void TemPrivilegio() {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("100");
		Assert.assertEquals(true, usuario.temPrivilegio());
	}
	
	@Test
	public void TempoDeUso() {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("100");
		Assert.assertEquals(7, usuario.calculaTempoDeUso());
	}
	
	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
}

