package br.com.trabalho.tests;

import java.util.Calendar;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;

public class EmprestimoTeste {

	public static void main(String[] args) throws Exception {
		executaSeeds();
		Livro livro = LivroDAO.buscaLivroCodigo("100");
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		Exemplar exemplar = livro.processaEmprestimo(usuario);
		new Emprestimo(usuario, exemplar);

		Usuario usuario2 = UsuarioDAO.buscaUsuarioCodigo("456");
		Exemplar exemplar2 = livro.processaEmprestimo(usuario2);
		new Emprestimo(usuario2, exemplar2);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 1995);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DATE, 29);
		if(Calendar.getInstance().getTime().after(cal.getTime())) {			
			System.out.println(cal.getTime());
		}
	}

	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
	
}
