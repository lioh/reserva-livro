package br.com.trabalho.tests;

import org.junit.jupiter.api.Test;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;
import org.junit.Assert;
import junit.framework.TestCase;

public class UsuarioTest extends TestCase{
	
	@Test
	public void IniciaSemEmprestimos() {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		Assert.assertEquals(0, usuario.getEmprestimos().size());
	}
	
	@Test
	public void IniciaSemReservas() {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		Assert.assertEquals(0, usuario.getReservas().size());
	}
	
	@Test
	public void ContadorDeEmprestimosIncrementa() throws Exception {
		executaSeeds();
		Livro livro = LivroDAO.buscaLivroCodigo("100");
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("456");
		Exemplar exemplar = livro.processaEmprestimo(usuario);
		Emprestimo emprestimo = new Emprestimo(usuario, exemplar);
		Assert.assertEquals(1, usuario.getEmprestimosAtivos().size());
	}
	
	@Test
	public void ContadorDeEmprestimosDecrementa() throws Exception {
		executaSeeds();
		Livro livro = LivroDAO.buscaLivroCodigo("100");
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("789");
		Exemplar exemplar = livro.processaEmprestimo(usuario);
		Emprestimo emprestimo = new Emprestimo(usuario, exemplar);
		usuario.devolveEmprestimo(emprestimo);
		Assert.assertEquals(0, usuario.getEmprestimosAtivos().size());
	}
	
	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
}

