package br.com.trabalho.tests;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;

public class DevolucaoTeste {

	public static void main(String[] args) throws Exception {
		executaSeeds();
		Livro livro = LivroDAO.buscaLivroCodigo("100");
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		Exemplar exemplar = livro.processaEmprestimo(usuario);
		Emprestimo emprestimo = new Emprestimo(usuario, exemplar);
		System.out.println(emprestimo.getExemplar().getStatus());
		System.out.println(usuario.getEmprestimos().size());
		System.out.println(livro.getEmprestimos().size());
		usuario.devolveEmprestimo(emprestimo);
		System.out.println(emprestimo.getExemplar().getStatus());
		System.out.println(usuario.getEmprestimos().size());
		System.out.println(livro.getEmprestimos().size());
	}

	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
	
}
