package br.com.trabalho.tests;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Reserva;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;

public class ReservaTeste {

	public static void main(String[] args) throws Exception {
		executaSeeds();
		Livro livro = LivroDAO.buscaLivroCodigo("100");
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		if(livro.processaReserva(usuario)) {
			livro.adicionaReserva(new Reserva(usuario, livro));
			System.out.println("");
			System.out.println("Reserva realizada com sucesso! :D");
			System.out.println("Para: "+ usuario.getNome()+", Titulo do Livro: "+ livro.getTitulo());
	
		} else {
			System.out.println("Existe exemplar disponível, iremos processar o empréstimo!");
			Exemplar exemplar = livro.processaEmprestimo(usuario);
			new Emprestimo(usuario, exemplar);
			System.out.println("");
			System.out.println("Emprestimo realizado com sucesso! :D");
			System.out.println("Para: "+ usuario.getNome()+", Titulo do Livro: "+ livro.getTitulo());
		}
		
		Exemplar exemplar = livro.processaEmprestimo(usuario);
		new Emprestimo(usuario, exemplar);
		
	}

	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
	
}
