package br.com.trabalho.tests;

import org.junit.jupiter.api.Test;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.StatusExemplar;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;
import org.junit.Assert;
import junit.framework.TestCase;

public class LivroTest extends TestCase{
	
	@Test
	public void ListaDeExemplaresFunciona() {
		executaSeeds();
		Livro livro = LivroDAO.buscaLivroCodigo("100");
		Assert.assertEquals(2, livro.getExemplares().size());
	}
	
	@Test
	public void ListaDeExemplaresEmprestadosAtualiza() throws Exception {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		Livro livro1 = LivroDAO.buscaLivroCodigo("100");
		Exemplar exemplar1 = livro1.processaEmprestimo(usuario);
		Emprestimo emprestimo1 = new Emprestimo(usuario, exemplar1);
		Assert.assertEquals(1, livro1.getExemplaresByStatus(StatusExemplar.EMPRESTADO).size());
	}
	
	@Test
	public void ListaDeExemplaresDisponiveisAtualiza() throws Exception {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("123");
		Livro livro1 = LivroDAO.buscaLivroCodigo("101");
		Exemplar exemplar1 = livro1.processaEmprestimo(usuario);
		Emprestimo emprestimo1 = new Emprestimo(usuario, exemplar1);
		Assert.assertEquals(0, livro1.getExemplaresByStatus(StatusExemplar.DISPONIVEL).size());
	}
	
	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
}

