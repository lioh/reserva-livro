package br.com.trabalho.tests;

import org.junit.jupiter.api.Test;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;
import org.junit.Assert;
import junit.framework.TestCase;

public class AlunoTest extends TestCase{
	
	@Test
	public void NaoAtingeMaximoComTresEmprestimos() throws Exception {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("456");
		Livro livro1 = LivroDAO.buscaLivroCodigo("100");
		Exemplar exemplar1 = livro1.processaEmprestimo(usuario);
		Emprestimo emprestimo1 = new Emprestimo(usuario, exemplar1);
		Livro livro2 = LivroDAO.buscaLivroCodigo("101");
		Exemplar exemplar2 = livro2.processaEmprestimo(usuario);
		Emprestimo emprestimo2 = new Emprestimo(usuario, exemplar2);
		Livro livro3 = LivroDAO.buscaLivroCodigo("200");
		Exemplar exemplar3 = livro3.processaEmprestimo(usuario);
		Emprestimo emprestimo3 = new Emprestimo(usuario, exemplar3);
				
		Assert.assertEquals(false, usuario.getMaxEmprestimos());
	}
	
	@Test
	public void AtingeMaximoComQuatroEmprestimos() throws Exception {
		executaSeeds();
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo("456");
		Livro livro1 = LivroDAO.buscaLivroCodigo("100");
		Exemplar exemplar1 = livro1.processaEmprestimo(usuario);
		Emprestimo emprestimo1 = new Emprestimo(usuario, exemplar1);
		Livro livro2 = LivroDAO.buscaLivroCodigo("101");
		Exemplar exemplar2 = livro2.processaEmprestimo(usuario);
		Emprestimo emprestimo2 = new Emprestimo(usuario, exemplar2);
		Livro livro3 = LivroDAO.buscaLivroCodigo("200");
		Exemplar exemplar3 = livro3.processaEmprestimo(usuario);
		Emprestimo emprestimo3 = new Emprestimo(usuario, exemplar3);
		Livro livro4 = LivroDAO.buscaLivroCodigo("201");
		Exemplar exemplar4 = livro4.processaEmprestimo(usuario);
		Emprestimo emprestimo4 = new Emprestimo(usuario, exemplar4);
				
		Assert.assertEquals(true, usuario.getMaxEmprestimos());
	}
	
	public static void executaSeeds () {
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
}

