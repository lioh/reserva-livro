package br.com.trabalho.views;

import java.util.Scanner;

import br.com.trabalho.commands.*;

import java.util.HashMap;

public class SistemaDeReserva {

	private static Scanner entrada;
	private static HashMap<String, Command> comandos = new HashMap<String, Command>();

	public static void main(String[] args) {
		boasVindas();
		inicializaComandos();
		while(true) {
			instrucoes();
			comandos.get("ver").execute(null);
			entrada = new Scanner(System.in);
			comandos.get(entrada.next()).execute(entrada);
			System.out.println();
		}
	}
	
	private static void inicializaComandos() {
		comandos.put("emp", new EmprestimoCommand());
		comandos.put("dev", new DevolverCommand());
		comandos.put("res", new ReservaCommand());
		comandos.put("obs", new ObservarCommand());
		comandos.put("liv", new ConsultarLivroCommand());
		comandos.put("usu", new ConsultarUsuarioCommand());
		comandos.put("ntf", new NotificarCommand());
		comandos.put("sai", new SaiCommand());
		comandos.put("ver", new VerificarCommand());
	}
	
	public static void boasVindas() {
		System.out.println("Olá, Seja bem-vindo!");
		System.out.println();
		System.out.println();
	}
	
	public static void instrucoes() {
		System.out.println("Para solicitar alguma operação você terá que digitar no console, "
				+ "conforme os exemplos abaixo:");
		System.out.println("");
		System.out.println("");
		System.out.println("emp codigo_usuario codigo_livro, para fazer um emprestimo de um livro.");
		System.out.println("dev codigo_usuario codigo_livro, para devolver um livro.");
		System.out.println("res codigo_usuario codigo_livro, para reservar um livro.");
		System.out.println("obs codigo_usuario codigo_livro, para observar um livro.");
		System.out.println("liv codigo_livro, consultar um livro.");
		System.out.println("usu codigo_usuario, consultar um usuario");
		System.out.println("ntf codigo_usuario, consultar notificações para um usuario");
		System.out.println("sai, para sair do programa.");
	}
}
