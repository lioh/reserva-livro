package br.com.trabalho.helpers;

import br.com.trabalho.errors.EmprestimoException;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;

public class ProfessorHelper implements UsuarioHelper {

	private Usuario usuario;
	
	public ProfessorHelper(Usuario usuario) {
		super();
		this.usuario = usuario;
	}

	@Override
	public boolean podeEmprestar(Livro livro) throws Exception {
		if( this.usuario.isEmprestado(livro)) {
			throw new EmprestimoException("Usuario já possui um exemplar emprestado.");
		} 
		return true;
	}
}
