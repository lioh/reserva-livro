package br.com.trabalho.helpers;

import br.com.trabalho.models.Livro;

public interface UsuarioHelper {
	boolean podeEmprestar(Livro livro) throws Exception;
}
