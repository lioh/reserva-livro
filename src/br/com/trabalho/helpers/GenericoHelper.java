package br.com.trabalho.helpers;

import br.com.trabalho.errors.EmprestimoException;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Usuario;

public class GenericoHelper implements UsuarioHelper{

	private Usuario usuario;
	
	public GenericoHelper(Usuario usuario) {
		super();
		this.usuario = usuario;
	}

	@Override
	public boolean podeEmprestar(Livro livro) throws Exception {
		if( this.usuario.isDevedor() ||
			this.usuario.getMaxEmprestimos() || 
			this.usuario.isEmprestado(livro)) {
			throw new EmprestimoException("Usuario não pode pegar um exemplar, "
					+ "Algumas causas possíveis:"
					+ "Usuario deve um livro na biblioteca, Usuario atingiu a"
					+ " quantidade máxima de emprestimos "
					+ "Usuario já possui um exemplar do livro emprestado");
		} 
		return true;	
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
