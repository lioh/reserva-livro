package br.com.trabalho.errors;

public class ReservaException extends AplicacaoException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReservaException(String error) {
		super(error);
	}
}
