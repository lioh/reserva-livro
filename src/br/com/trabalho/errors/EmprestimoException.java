package br.com.trabalho.errors;

public class EmprestimoException extends AplicacaoException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmprestimoException(String error) {
		super(error);
	}

}
