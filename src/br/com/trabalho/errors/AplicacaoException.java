package br.com.trabalho.errors;

public class AplicacaoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AplicacaoException(String error) {
		super("Algo de errado aconteceu com sua solicitação :( "
				+ "Olha só o problemaço que deu ----> " + error);
	}
	
}
