package br.com.trabalho.errors;

public class DevolveException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DevolveException(String error) {
		super(error);
	}
}
