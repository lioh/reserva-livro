package br.com.trabalho.seeds;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.models.Livro;

public class LivroSeed {
	
	private LivroSeed() {}
	private static LivroSeed livroSeed;
	
	public static void geraDados() {
		
		if(livroSeed == null) {
		
			Livro livro = new Livro();
			livro.setCodigo("100");
			livro.setTitulo("Engenharia de Software");
			livro.setEditora("AddisonWesley");
			livro.adicionaAutores("Ian Sommervile");
			livro.setEdicao("6a");
			livro.setAnoPublicacao("2000");
			LivroDAO.adicionaLivro(livro);
			
			Livro livro1 = new Livro();
			livro1.setCodigo("101");
			livro1.setTitulo("UML – Guia do Usuário");
			livro1.setEditora("Campus");
			livro1.adicionaAutores("Grady Booch");
			livro1.adicionaAutores("James Rumbaugh");
			livro1.adicionaAutores("Ivar Jacobso");
			livro1.setEdicao("7a");
			livro1.setAnoPublicacao("2000");
			LivroDAO.adicionaLivro(livro1);
			
			Livro livro2 = new Livro();
			livro2.setCodigo("200");
			livro2.setTitulo("Code Complete");
			livro2.setEditora("Microsoft Press");
			livro2.adicionaAutores("Steve McConnell");
			livro2.setEdicao("2a");
			livro2.setAnoPublicacao("2014");
			LivroDAO.adicionaLivro(livro2);
			
			Livro livro3 = new Livro();
			livro3.setCodigo("201");
			livro3.setTitulo("Agile Software Development, Principles, Patterns and Practices");
			livro3.setEditora("Prentice Hall");
			livro3.adicionaAutores("Robert Martin");
			livro3.setEdicao("1a");
			livro3.setAnoPublicacao("2002");
			LivroDAO.adicionaLivro(livro3);
			
			Livro livro4 = new Livro();
			livro4.setCodigo("300");
			livro4.setTitulo("Refactoring Improving the Design of Existing Code");
			livro4.setEditora("Addison-Wesley Professional");
			livro4.adicionaAutores("Martin Fowler");
			livro4.setEdicao("1a");
			livro4.setAnoPublicacao("1999");
			LivroDAO.adicionaLivro(livro4);
			
			Livro livro5 = new Livro();
			livro5.setCodigo("301");
			livro5.setTitulo("Software Metrics: Rigorous and Practical Approach");
			livro5.setEditora("CRC Press");
			livro5.adicionaAutores("Norman Fenton");
			livro5.adicionaAutores("James Bieman");
			livro5.setEdicao("3a");
			livro5.setAnoPublicacao("2014");
			LivroDAO.adicionaLivro(livro5);
			
			Livro livro6 = new Livro();
			livro6.setCodigo("400");
			livro6.setTitulo("Design Patterns: Elements of Reusable Object-Oriented Software");
			livro6.setEditora("Addison-Wesley Professional");
			livro6.adicionaAutores("Erich Gamma");
			livro6.adicionaAutores("Richard Helm");
			livro6.adicionaAutores("Ralph Johnson");
			livro6.adicionaAutores("John Vlissides");
			livro6.setEdicao("1a");
			livro6.setAnoPublicacao("1994");
			LivroDAO.adicionaLivro(livro6);
		
			Livro livro7 = new Livro();
			livro7.setCodigo("401");
			livro7.setTitulo("UML Distilled: Brief Guide to the Standard Object Modeling Language");
			livro7.setEditora("Addison-Wesley Professional");
			livro7.adicionaAutores("Martin Fowler");
			livro7.setEdicao("3a");
			livro7.setAnoPublicacao("");
			LivroDAO.adicionaLivro(livro7);
			
			livroSeed = new LivroSeed();
			System.out.println("Dados dos livros gerados com sucesso!");
			System.out.println("");
		} 
	}
}
