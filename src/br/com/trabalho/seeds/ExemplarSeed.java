package br.com.trabalho.seeds;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.StatusExemplar;

public class ExemplarSeed {

	private ExemplarSeed() {}
	private static ExemplarSeed exemplarSeed;
	
	public static void geraDados() {
		if(exemplarSeed == null) {
			
			Exemplar exemplar = new Exemplar();
			exemplar.setCodigo("01");
			exemplar.setStatus(StatusExemplar.DISPONIVEL);
			exemplar.setLivro(LivroDAO.buscaLivroCodigo("100"));
			
			Exemplar exemplar2 = new Exemplar();
			exemplar2.setCodigo("02");
			exemplar2.setStatus(StatusExemplar.DISPONIVEL);
			exemplar2.setLivro(LivroDAO.buscaLivroCodigo("100"));
			
			Exemplar exemplar3 = new Exemplar();
			exemplar3.setCodigo("03");
			exemplar3.setStatus(StatusExemplar.DISPONIVEL);
			exemplar3.setLivro(LivroDAO.buscaLivroCodigo("101"));
			
			Exemplar exemplar4 = new Exemplar();
			exemplar4.setCodigo("04");
			exemplar4.setStatus(StatusExemplar.DISPONIVEL);
			exemplar4.setLivro(LivroDAO.buscaLivroCodigo("200"));
			
			Exemplar exemplar5 = new Exemplar();
			exemplar5.setCodigo("05");
			exemplar5.setStatus(StatusExemplar.DISPONIVEL);
			exemplar5.setLivro(LivroDAO.buscaLivroCodigo("201"));
			
			Exemplar exemplar6 = new Exemplar();
			exemplar6.setCodigo("06");
			exemplar6.setStatus(StatusExemplar.DISPONIVEL);
			exemplar6.setLivro(LivroDAO.buscaLivroCodigo("300"));
			
			Exemplar exemplar7 = new Exemplar();
			exemplar7.setCodigo("07");
			exemplar7.setStatus(StatusExemplar.DISPONIVEL);
			exemplar7.setLivro(LivroDAO.buscaLivroCodigo("300"));

			Exemplar exemplar8 = new Exemplar();
			exemplar8.setCodigo("08");
			exemplar8.setStatus(StatusExemplar.DISPONIVEL);
			exemplar8.setLivro(LivroDAO.buscaLivroCodigo("400"));
			
			Exemplar exemplar9 = new Exemplar();
			exemplar9.setCodigo("09");
			exemplar9.setStatus(StatusExemplar.DISPONIVEL);
			exemplar9.setLivro(LivroDAO.buscaLivroCodigo("400"));
			
			exemplarSeed = new ExemplarSeed();
			System.out.println("Dados dos exemplares gerados com sucesso!");
			System.out.println("");
		} 
	}
}