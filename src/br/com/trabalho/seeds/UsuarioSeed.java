package br.com.trabalho.seeds;

import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.helpers.GenericoHelper;
import br.com.trabalho.helpers.ProfessorHelper;
import br.com.trabalho.models.Aluno;
import br.com.trabalho.models.Funcionario;
import br.com.trabalho.models.Professor;
import br.com.trabalho.models.Usuario;

public class UsuarioSeed {
	
	private UsuarioSeed() {}
	private static UsuarioSeed usuarioSeed;
	
	public static void geraDados() {		
		if(usuarioSeed == null) {
			
			Usuario usuario = new Funcionario();
			usuario.setNome("João da Silva");
			usuario.setCodigo("123");
			usuario.setUsuarioHelper(new GenericoHelper(usuario));
			UsuarioDAO.adicionaUsuario(usuario);
			
			Usuario usuario2 = new Aluno();
			usuario2.setNome("Luiz Fernando Rodrigues");
			usuario2.setCodigo("456");
			usuario2.setUsuarioHelper(new GenericoHelper(usuario2));
			UsuarioDAO.adicionaUsuario(usuario2);
		
			Usuario usuario3 = new Funcionario();
			usuario3.setNome("Pedro Paulo");
			usuario3.setCodigo("789");
			usuario3.setUsuarioHelper(new GenericoHelper(usuario3));
			UsuarioDAO.adicionaUsuario(usuario3);
		

			Usuario usuario4 = new Professor();
			usuario4.setNome("Carlos Lucena");
			usuario4.setCodigo("100");
			usuario4.setUsuarioHelper(new ProfessorHelper(usuario4));
			UsuarioDAO.adicionaUsuario(usuario4);
			
			usuarioSeed = new UsuarioSeed();
			System.out.println("Dados dos usuarios gerados com sucesso!");
			System.out.println("");
		}
	}
	

}
