package br.com.trabalho.controllers;

import br.com.trabalho.daos.LivroDAO;
import br.com.trabalho.daos.UsuarioDAO;
import br.com.trabalho.models.Emprestimo;
import br.com.trabalho.models.Exemplar;
import br.com.trabalho.models.Livro;
import br.com.trabalho.models.Observer;
import br.com.trabalho.models.Reserva;
import br.com.trabalho.models.Usuario;
import br.com.trabalho.seeds.ExemplarSeed;
import br.com.trabalho.seeds.LivroSeed;
import br.com.trabalho.seeds.UsuarioSeed;

public class AplicacaoController {
	private static AplicacaoController controller;
	
	private AplicacaoController() {
		super();
		LivroSeed.geraDados();
		ExemplarSeed.geraDados();
		UsuarioSeed.geraDados();
	}
	
	public static AplicacaoController getController() {
		if(controller == null) {
			return new AplicacaoController();
		} else {
			return controller;
		}
		
	} 
	public void verificarAtraso() {
		for (Livro livro : LivroDAO.getLivros()) {
			livro.verificaAtraso();
		}
	}
	
	public void emprestar(String codigoUsuario, String codigoLivro) {		
		try {			
			Usuario usuario = UsuarioDAO.buscaUsuarioCodigo(codigoUsuario);
			Livro livro = LivroDAO.buscaLivroCodigo(codigoLivro);
			Exemplar exemplar = livro.processaEmprestimo(usuario);
			new Emprestimo(usuario, exemplar);
			System.out.println("");
			System.out.println("Emprestimo realizado com sucesso! :D");
			System.out.println("Para: "+ usuario.getNome()+", Titulo do Livro: "+ livro.getTitulo());
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
	}

	public void reservar(String codigoUsuario, String codigoLivro) {
		try {			
			Usuario usuario = UsuarioDAO.buscaUsuarioCodigo(codigoUsuario);
			Livro livro = LivroDAO.buscaLivroCodigo(codigoLivro);
			if(livro.processaReserva(usuario)) {
				new Reserva(usuario, livro);
				System.out.println("");
				System.out.println("Reserva realizada com sucesso! :D");
				System.out.println("Para: "+ usuario.getNome()+", Titulo do Livro: "+ livro.getTitulo());
		
			} else {
				System.out.println("Existe exemplar disponível, iremos processar o empréstimo!");
				this.emprestar(codigoUsuario, codigoLivro);
			}
		
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
	}
	
	public void devolver(String codigoUsuario, String codigoLivro) {
		try {			
			Usuario usuario = UsuarioDAO.buscaUsuarioCodigo(codigoUsuario);
			Livro livro = LivroDAO.buscaLivroCodigo(codigoLivro);
			Emprestimo emprestimo = usuario.getLivroEmprestado(livro);
			usuario.devolveEmprestimo(emprestimo);
			System.out.println("");
			System.out.println("Devolução realizado com sucesso! :D");
			System.out.println("Para: "+ usuario.getNome()+", Titulo do Livro: "+ livro.getTitulo());
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
	}	
	
	public void observar(String codigoUsuario, String codigoLivro) {
		try {			
			Usuario usuario = UsuarioDAO.buscaUsuarioCodigo(codigoUsuario);
			Livro livro = LivroDAO.buscaLivroCodigo(codigoLivro);
			livro.registraObservador((Observer) usuario);
			System.out.println("");
			System.out.println("Observador adicionado com sucesso! :D");
			System.out.println("Para: "+ usuario.getNome()+", Titulo do Livro: "+ livro.getTitulo());
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
	}
	
	public void consultaLivro(String codigoLivro) {		
		LivroDAO.consultaLivro(codigoLivro);
	}
	
	public void consultaUsuario(String codigoUsuario) {
		UsuarioDAO.consultaUsuario(codigoUsuario);
	}

	public void consultaNotificacaoUsuario(String codigoUsuario) {
		Usuario usuario = UsuarioDAO.buscaUsuarioCodigo(codigoUsuario);
		int numero = LivroDAO.consultaNotificacaoUsuario(usuario);
		System.out.println("Quantidade de notificações: "+numero);
	}
}
