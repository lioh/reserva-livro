Projeto desenvolvido como trabalho final da disciplina de Engenharia de Software I.
O projeto se trata de um sistema de biblioteca que disponibiliza as seguintes funcionalidades:

Empréstimo de exemplares;

Reserva de exemplares;

Devolução;

Cadastro de livros, usuarios e exemplares;

Consultas de emprestimos e reservas.

Foram implementados alguns padrões de projetos que foram abordados ao longo da disciplina, como:

Observer;

Strategy;

Façade;

Command;

Singleton;

State.

Dentre outros padões implementados. 